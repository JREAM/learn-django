# Learn Django

## Install Instructions

Create our virtual environment

    $ virtualenv venv

Activate Virtual Environment in Windows

    $ source venv/Scripts/activate

Activate Virtual Environment in Linux

    $ source venv/bin/activate

Install Required Packages

    $ pip install -r requirements.pip

## Create a Super User
You need a super user to log in to the admin area, located at /admin

    $ ./manage.py createsuperuser

## Options

Leave the Virtual Environment

    $ deactivate

# How Settings Work

Order of Loading

    - local.py (from init.py, load below)
        - development.py
            - base.py

    - local.py (from init.py, load below)
        - production.py
            - base.py

# Default Super User

    admin:admin