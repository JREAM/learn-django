$(function() {

    $("#sortable").sortable({
        axis: "y",
        placeholder: "ui-state-highlight",
        cursor: "move"
    });
    $("#sortable").disableSelection();

    $("#nba_teampick_save").click(function(evt) {
        var postData = {
            postData: JSON.stringify($("#sortable").sortable("toArray"))
        }
        console.log(postData)

        $.post("/nba_teampick/select/", postData, function(data) {
            console.log(data)
        }, 'json');
    });

});