from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns(
    '',
    url(r'^$', 'landings.views.home', name='home'),
    url(r'^nba_teampick/', include('nba_teampick.urls')),
    url(r'^blog/', include('blog.urls')),
    url(r'^about$', 'landings.views.about'),
    url(r'^admin/', include(admin.site.urls)),
)
