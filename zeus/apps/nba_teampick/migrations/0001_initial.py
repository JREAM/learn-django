# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('nba', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Picks',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('season', models.SmallIntegerField(default=2015)),
                ('rank', models.SmallIntegerField(default=0)),
                ('team', models.ForeignKey(to='nba.Teams')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='picks',
            unique_together=set([('user', 'season', 'team'), ('user', 'season', 'rank')]),
        ),
    ]
