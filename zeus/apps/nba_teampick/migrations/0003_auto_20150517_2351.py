# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('nba_teampick', '0002_auto_20150511_0220'),
    ]

    operations = [
        migrations.AlterField(
            model_name='picks',
            name='season',
            field=models.ForeignKey(to='nba.Seasons'),
        ),
        migrations.AlterField(
            model_name='status',
            name='season',
            field=models.ForeignKey(to='nba.Seasons'),
        ),
        migrations.DeleteModel(
            name='Seasons',
        ),
    ]
