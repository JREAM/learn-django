# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('nba_teampick', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Seasons',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start', models.SmallIntegerField(blank=True)),
                ('end', models.SmallIntegerField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('locked', models.BooleanField(default=0)),
                ('season', models.ForeignKey(to='nba_teampick.Seasons')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterModelOptions(
            name='picks',
            options={'verbose_name': 'pick', 'verbose_name_plural': 'Picks'},
        ),
        migrations.AlterField(
            model_name='picks',
            name='season',
            field=models.ForeignKey(to='nba_teampick.Seasons'),
        ),
    ]
