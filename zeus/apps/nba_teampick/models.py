from django.db import models
from django.conf import settings
from nba.models import Teams, Seasons


class Picks(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    season = models.ForeignKey(Seasons)
    team = models.ForeignKey(Teams)
    rank = models.SmallIntegerField(default=0)

    class Meta:
        unique_together = (
            ('user', 'season', 'team'),
            ('user', 'season', 'rank')
        )
        verbose_name = "pick"
        verbose_name_plural = "Picks"

    def __unicode__(self):
        return self.user


class Status(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    season = models.ForeignKey(Seasons)
    locked = models.BooleanField(default=0)

    verbose_name = "status"
    verbose_name_plural = "Statuses"

    def __unicode__(self):
        return self.user
