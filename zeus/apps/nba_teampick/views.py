import json
import re

from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from .models import Picks
from nba.models import Teams


@login_required
def teampick_dashboard(request):
    context = {
        'picks': Picks.objects.filter(user=request.user),
    }

    return render(request, 'teampick_dashboard.html', context)


@login_required
@csrf_exempt
def teampick_select(request):
    if request.method == 'GET':
        context = {
            'picks': Picks.objects.filter(user=request.user),
            'teams': Teams.objects.all().order_by('picks__rank')
        }

        return render(request, 'teampick_select.html', context)

    # Save the users order of team picks
    postData = request.POST.get('postData', False)

    output = {
        'result': 1,
        'error': False,
        'data': False
    }

    # If there is no Data Stop Here
    if not postData:
        output['result'] = 0
        output['error'] = 'No Post Data'

        return HttpResponse(
            json.dumps(output),
            content_type="application/json"
        )

    # Otherwise Continue Posting the Data
    team_list = json.loads(postData)
    bulk = []
    for key, id in enumerate(team_list):
        team_id = re.sub('[^0-9]', '', id)
        bulk.append(Picks(
            user=request.user,
            team_id=team_id,
            season_id=2015,
            rank=key
        ))

    Picks.objects.filter(user=request.user).delete()
    Picks.objects.bulk_create(bulk)

    return HttpResponse(
        json.dumps(output),
        content_type="application/json"
    )


@login_required
def teampick_compare(request):
    context = {
        'picks': Picks.objects.filter(user=request.user),
    }

    return render(request, 'teampick_compare.html', context)
