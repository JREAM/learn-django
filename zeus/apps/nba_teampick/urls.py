from django.conf.urls import patterns, include, url

urlpatterns = patterns(
    'nba_teampick.views',
    url(r'^$', 'teampick_dashboard', name="teampick_dashboard"),
    url(r'^select/$', 'teampick_select', name="teampick_select"),
    url(r'^compare/$', 'teampick_compare', name="teampick_compare"),
)
