from django.contrib import admin
from .models import (
    Seasons,
    Teams,
    TeamScores,
)


class SeasonsAdmin(admin.ModelAdmin):
    fields = ('name', 'year', 'start', 'end')
    list_display = ('name', 'year', 'start', 'end')


class TeamsAdmin(admin.ModelAdmin):
    fields = ('location', 'name')
    list_display = ('location', 'name', 'slug', 'nba_id')
    readonly_fields = ('location', 'name', 'slug',)


class TeamScoresAdmin(admin.ModelAdmin):
    list_display = ('team', 'season', 'wins', 'losses')
    readonly_fields = ('team', 'season', 'wins', 'losses')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request):
        return False


admin.site.register(Seasons, SeasonsAdmin)
admin.site.register(Teams, TeamsAdmin)
admin.site.register(TeamScores, TeamScoresAdmin)
