from django.db import models
from autoslug import AutoSlugField


class Seasons(models.Model):
    name = models.CharField(max_length=100)
    year = models.SmallIntegerField(unique=True)
    start = models.DateField(null=True, blank=True)
    end = models.DateField(null=True, blank=True)

    class Meta:
        ordering = ['name']
        verbose_name = "season"
        verbose_name_plural = "seasons"

    def __unicode__(self):
        return self.name


class Teams(models.Model):
    name = models.CharField(max_length=100)
    location = models.CharField(max_length=100)
    slug = AutoSlugField(populate_from='title', unique=True)

    # If a team is inactive, we could lock it for legacy data
    active = models.BooleanField(default=1)

    # Applies to NBA.com stats API
    nba_id = models.SmallIntegerField(default=0, unique=True)

    class Meta:
        ordering = ['name']
        verbose_name = "team"
        verbose_name_plural = "teams"

    def __unicode__(self):
        return self.name


class TeamScores(models.Model):
    team = models.ForeignKey(Teams)
    season = models.ForeignKey(Seasons)
    wins = models.SmallIntegerField(default=0)
    losses = models.SmallIntegerField(default=0)

    def __unicode__(self):
        return self.team
