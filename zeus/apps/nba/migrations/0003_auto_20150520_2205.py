# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('nba', '0002_auto_20150517_2351'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='seasons',
            options={'ordering': ['name'], 'verbose_name': 'season', 'verbose_name_plural': 'seasons'},
        ),
        migrations.RenameField(
            model_name='seasons',
            old_name='nba_id',
            new_name='year',
        ),
        migrations.AlterField(
            model_name='seasons',
            name='end',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='seasons',
            name='start',
            field=models.DateField(null=True, blank=True),
        ),
    ]
