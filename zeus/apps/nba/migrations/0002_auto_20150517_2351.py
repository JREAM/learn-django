# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('nba', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Seasons',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('start', models.SmallIntegerField(blank=True)),
                ('end', models.SmallIntegerField(blank=True)),
                ('nba_id', models.SmallIntegerField(unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='TeamScores',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('wins', models.SmallIntegerField(default=0)),
                ('losses', models.SmallIntegerField(default=0)),
                ('season', models.ForeignKey(to='nba.Seasons')),
            ],
        ),
        migrations.AddField(
            model_name='teams',
            name='active',
            field=models.BooleanField(default=1),
        ),
        migrations.AddField(
            model_name='teams',
            name='nba_id',
            field=models.SmallIntegerField(default=0, unique=True),
        ),
        migrations.AddField(
            model_name='teamscores',
            name='team',
            field=models.ForeignKey(to='nba.Teams'),
        ),
    ]
