import sys
import time
import urllib2
import json

from django.core.management.base import (
    BaseCommand,
    CommandError
)

from nba.models import (
    Seasons,
    Teams,
    TeamScores,
)


class Command(BaseCommand):

    def handle(self, *args, **options):

        day_offset = '0'
        league_id = '00'
        game_date = time.strftime("%m/%d/%Y")

        query_string = 'scoreboard?DayOffset=%s&LeagueID=%s&gameDate=%s' % (
            day_offset,
            league_id,
            game_date
        )
        url = 'http://stats.nba.com/stats/' + query_string

        try:
            request = urllib2.Request(url)
            response = urllib2.urlopen(request)
        except urllib2.URLError as e:
            sys.exit("URLError fetching feed (%s) (%s)" % (url, e))
        except urllib2.HTTPError as e:
            sys.exit("HTTPError fetching feed (%s) (%s)" % (url, e))

        result = response.read()
        if len(result) == 0:
            sys.exit("Error, empty response.")

        result = json.loads(result)
        del result["resultSets"][0:4]
        result["resultSets"].pop()

        # East Conference
        print "Collecting East Conference:\n----------------------------------"
        for data in result["resultSets"][0]["rowSet"]:
            self.save_score(data)

        # West Conference
        print "Collecting West Conference:\n----------------------------------"
        for data in result["resultSets"][1]["rowSet"]:
            self.save_score(data)

    def save_score(self, data):
        current_year = time.strftime("%Y")

        team_id = int(data[0])
        team_name = data[5]

        wins = int(data[7])
        losses = int(data[8])

        try:
            season = Seasons.objects.get(year=current_year)
        except Seasons.DoesNotExist:
            print 'Season not found: %s' % current_year
            return

        try:
            team = Teams.objects.get(nba_id=team_id)
        except Teams.DoesNotExist:
            print 'Team not found: [%d] %s' % (team_id, team_name)
            return

        try:
            # See if a record exists, if so update!
            team_score = TeamScores.objects.get(
                team=team,
                season=season
            )
        except TeamScores.DoesNotExist:
            # Create a new Record
            team_score = TeamScores()

        team_score.team = team
        team_score.season = season
        team_score.wins = wins
        team_score.losses = losses
        team_score.save()

        print "Team [%d] %s: wins %d - losses %d" % (
            team_id,
            team_name,
            wins,
            losses
        )
