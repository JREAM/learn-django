from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Categories, Posts


def blog_home(request):

    paginator = Paginator(Posts.objects.filter(published=True), 8)
    page = request.GET.get('page')

    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)

    context = {
        'categories': Categories.objects.all(),
        'posts': posts
    }

    return render(request, 'blog_home.html', context)


def blog_post(request, slug):
    context = {
        'post': get_object_or_404(Posts, slug=slug)
    }

    return render(request, 'blog_post.html', context)


def blog_category(request, slug):
    category = get_object_or_404(Categories, slug=slug)
    context = {
        'category': category,
        'posts': Posts.objects.filter(published=True, category=category)
    }

    return render(request, 'blog_category.html', context)
