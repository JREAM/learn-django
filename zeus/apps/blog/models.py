from django.db import models
from autoslug import AutoSlugField


class Categories(models.Model):
    title = models.CharField(max_length=100)
    content = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    slug = AutoSlugField(populate_from='title', unique=True)

    class Meta:
        ordering = ['title']
        verbose_name = "category"
        verbose_name_plural = "categories"

    def __unicode__(self):
        return self.title


class Posts(models.Model):
    title = models.CharField(max_length=100)
    category = models.ForeignKey(Categories)
    published = models.BooleanField(default=0)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    slug = AutoSlugField(populate_from='title', always_update=True, unique=True)

    class Meta:
        ordering = ['-created_at', 'title']
        verbose_name = "post"
        verbose_name_plural = "posts"

    def __unicode__(self):
        return self.title
