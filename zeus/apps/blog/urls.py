from django.conf.urls import patterns, include, url

urlpatterns = patterns(
    'blog.views',
    url(r'^$', 'blog_home', name="blog_home"),
    url(r'^post/(?P<slug>[\w-]+)/$', 'blog_post', name="blog_post"),
    url(r'^category/(?P<slug>[\w-]+)/$', 'blog_category', name="blog_category"),
)
