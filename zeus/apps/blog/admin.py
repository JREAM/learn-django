from django.contrib import admin
from .models import Categories, Posts


class CategoriesAdmin(admin.ModelAdmin):
    fields = ('title', 'content')
    list_display = ('title', 'slug', 'created_at', 'updated_at')
    readonly_fields = ('slug',)


class PostsAdmin(admin.ModelAdmin):
    fields = ('title', 'category', 'content', 'published')
    list_display = ('title', 'slug', 'published', 'category', 'created_at', 'updated_at')
    search_fields = ['title', 'slug', 'category__slug', 'category__title']
    readonly_fields = ('slug',)



admin.site.register(Categories, CategoriesAdmin)
admin.site.register(Posts, PostsAdmin)
