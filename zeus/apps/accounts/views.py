from django.shortcuts import render, redirect

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt

from accounts.models import Account


def account_login(request):
    """ Dan Sackett Style """
    error = None

    if request.method == 'POST':
        account = authenticate(
            username=request.POST.get('username'),
            password=request.POST.get('password')
        )

        if account and account.is_active:
            login(request, account)
            return redirect('')

        error = 'Invalid Credentials.'

    context = {
        'error': error
    }
    return render(request, 'login.html', context)


@login_required
@csrf_exempt
def account_logout(request):
    logout(request)
    redirect('home')


@login_required
def view_account(request, username):
    account = Account.objects.get(username=username)
    context = {
        'account': account
    }

    return render(request, '', context)


@login_required
def edit_account(request):
    account = request.user
    pass
