from django.contrib import admin
from .models import Account


class AccountAdmin(admin.ModelAdmin):
    fields = ('is_admin', 'username', 'email', 'first_name', 'last_name')
    list_display = ('is_admin', 'username', 'email', 'first_name', 'last_name')

admin.site.register(Account, AccountAdmin)
